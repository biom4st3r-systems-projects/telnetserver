from typing import Any, Iterable, List, Tuple, Union
from cryptography.hazmat.primitives import ciphers
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric.rsa import RSAPublicNumbers
from cryptography.hazmat.primitives.hmac import HMAC
from cryptography.hazmat.primitives.hashes import HashAlgorithm
from cryptography.hazmat.primitives.ciphers import Cipher, CipherContext, algorithms, modes
from objs.CipherWrapper import CipherWrapper
from objs.TypeCheck import checkType
from os import SEEK_END, urandom
from io import BytesIO
from zlib import compress, decompress


class Packet:
    def __init__(self, data: bytes, strip=False) -> None:
        self._raw: bytes = None
        checkType(data, bytes)
        self._data: BytesIO = BytesIO(data)
        self._encrypted: bytes = None
        self.mac: bytes = None
        if strip:
            self.strip()

    def strip(self):
        if self._raw:
            raise Exception('Packet was already striped')
        self._raw = self._data.getvalue()
        data = self._data.getvalue()[4:]
        padding_size = data[0]
        # padding = data[-padding_size:]
        # self.bad_padding == b'\x00' * len(padding)
        data = data[1:-padding_size]
        self._data = BytesIO(data)

    def getuint32(self):
        return int.from_bytes(self._data.read(4), 'big', signed=False)

    def getString(self) -> str:
        return self.getBytes().decode('utf-8')

    def getMPint(self) -> int:
        return int.from_bytes(self.getBytes(), 'big', signed=False)

    def getBytes(self) -> bytes:
        return self._data.read(self.getuint32())

    def getByte(self) -> int:
        return self._data.read(1)[0]

    def getNameList(self) -> List[str]:
        return self.getString().split(',')

    def getBoolean(self) -> bool:
        return self.getByte() > 0

    def _get(self, length: int) -> bytes:
        return self._data.read(length)

    def decrypt(self, cipher: CipherWrapper, hash: Tuple[hashes.HashAlgorithm, int]) -> 'Packet':
        self._encrypted = bytearray(self._data.getvalue())

        data = self._data.getvalue()
        mac = data[-hash[1]:]
        data = data[:-hash[1]]  # Must remove the mac or my decryptor falls out of sync with the client
        data = cipher.decrypt(data)
        # data = data[:int.from_bytes(data[:4], 'big', signed=False) + 4]
        self.mac = mac
        self._data = BytesIO(data)
        return self

    def _reset_index(self) -> None:
        self._data.seek(0)

    def compute_mac(self, protomac: Tuple[bytes, int], hasher: HashAlgorithm):
        if hasher:
            builder = PacketBuilder()._adduint32(protomac[1])._addBytes(self._data if not self._raw else self._raw)
            hmac = HMAC(protomac[0], hasher)
            hmac.update(builder.data())
            hmac = hmac.finalize()
            return hmac
        return b''

    def _get_length(self):
        length = self._data.seek(0, SEEK_END)
        self._reset_index()
        return length


class PacketBuilder:
    '''An (often misused) byte builder for packets. Underscored methods don't add a length before content'''
    def __init__(self):
        self._data: bytearray = bytearray()
        self._encrypted: bytearray = None
        self.packeted = False

    def addString(self, string: Union[str, bytes]) -> 'PacketBuilder':
        self._adduint32(len(string))
        self._data.extend(string if isinstance(string, bytes) else string.encode('utf-8'))
        return self

    def addNameList(self, val: Union[str, Iterable]) -> 'PacketBuilder':
        if isinstance(val, Iterable):
            val = ','.join(val).encode('utf-8')
        else:
            val = val.encode('utf-8')
        self._adduint32(len(val))
        self._data.extend(val)
        return self

    def addSections(self, sections: Iterable[Any]) -> 'PacketBuilder':
        data = b''
        for x in sections:
            if isinstance(x, int):
                x = PacketBuilder.remove_zero_padding(x.to_bytes(x.bit_length() + 7, 'big', signed=True))
            data += len(x).to_bytes(4, 'big', signed=True)
            if isinstance(x, str):
                data += x.encode('utf-8')
            elif isinstance(x, bytes):
                data += x
            elif isinstance(x, int):
                x = PacketBuilder.remove_zero_padding(x.to_bytes(x.bit_length() + 7, 'big', signed=True))
        self.addString(data)
        return self

    def _addBoolean(self, boolean: bool) -> 'PacketBuilder':
        self._data.extend(int.to_bytes(boolean, 1, 'big'))
        return self

    @staticmethod
    def remove_zero_padding(val: bytes):
        i = 0
        while val[i] == 0:
            i += 1
        return val[i:]

    def addmpint(self, val: int) -> 'PacketBuilder':
        checkType(val, int)
        bitlen = val.bit_length()
        val: bytes = val.to_bytes(val.bit_length() + 7, 'big', signed=False)
        val = PacketBuilder.remove_zero_padding(val)
        if (bitlen % 8) == 0:    # FIXME: Gross haxs TODO: Why?
            val = b'\x00' + val
        elif (bitlen % 8) == 4:  # FIXME: Gross haxs TODO: Why?
            val = val[2:]
        self.addString(val)
        return self

    def _addByte(self, val: Union[bytes, int]) -> 'PacketBuilder':
        if isinstance(val, int):
            val = val.to_bytes(1, 'big', signed=True)
        assert len(val) == 1
        self._data.extend(val)
        return self

    def _adduint32(self, val: int) -> 'PacketBuilder':
        checkType(val, int)
        self._data.extend(int.to_bytes(val, 4, 'big'))
        return self

    def _addBytes(self, val: bytes) -> 'PacketBuilder':
        checkType(val, bytes)
        self._data.extend(val)
        return self

    def data(self) -> bytes:
        return bytes(self._data if not self._encrypted else self._encrypted)

    def len(self) -> int:
        return len(self._data)

    def addCertificate(self, id: str, data: bytes) -> 'PacketBuilder':
        self.addString(PacketBuilder().addString(id).addString(data).data())
        return self

    def addRsaPubKey(self, numbers: RSAPublicNumbers, id: str = 'ssh-rsa') -> 'PacketBuilder':
        self.addString(PacketBuilder().addString(id).addmpint(numbers.e).addmpint(numbers.n).data())
        return self

    '''RFC 4253 Section 6 - random padding
         Arbitrary-length padding, such that the total length of
         (packet_length || padding_length || payload || random padding)
         is a multiple of the cipher block size or 8, whichever is larger.'''
    def packetize(self, synth_padding: Union[bytes, str] = None, padding_modulo: int = 8) -> 'PacketBuilder':
        padding_len = 4
        # len of packet + uint32 size + min_padding + padding length byte
        packet_length = len(self._data) + 4 + padding_len + 1
        extra_padding = padding_modulo - ((packet_length % padding_modulo) or padding_modulo)
        packet_length += extra_padding
        padding_len += extra_padding
        builder = PacketBuilder(
            )._adduint32(packet_length - 4  # minus the length of size
            )._addByte(padding_len
            )._addBytes(self.data())
        if not synth_padding:
            builder._addBytes(urandom(padding_len))
        else:
            if isinstance(synth_padding, str):
                synth_padding = synth_padding.encode('utf-8')
            synth_padding = synth_padding[:padding_len]
            while len(synth_padding) < padding_len:
                synth_padding = synth_padding * 2
            builder._addBytes(synth_padding[:padding_len])
        self._data = builder._data
        self.packeted = True
        return self

    def _getMac(self, proto_mac, hasher):
        builder = PacketBuilder()._adduint32(proto_mac[1])._addBytes(self.data())
        hmac = HMAC(proto_mac[0], hasher)
        hmac.update(builder.data())
        mac = hmac.finalize()

        return mac

    def _addMac(self, proto_mac: Tuple[bytes, int], hasher: HashAlgorithm):
        checkType(proto_mac[0], bytes)
        checkType(proto_mac[1], int)
        if hasher:
            builder = PacketBuilder()._adduint32(proto_mac[1])._addBytes(bytes(self._data))
            hmac = HMAC(proto_mac[0], hasher)
            hmac.update(builder.data())
            mac = hmac.finalize()
            if self._encrypted:
                self._encrypted.extend(mac)
            else:
                raise AssertionError('Packet hasn\'t been encrypted yet! you shouldn\'t be adding a mac')
        return self

    def _encrypt(self, cipher: CipherWrapper) -> 'PacketBuilder':
        self._encrypted = bytearray(cipher.encrypt(self.data()))
        return self

    def encryptAndAddMac(self, cipher: CipherWrapper, proto_mac: Tuple[bytes, int], hasher: HashAlgorithm) -> 'PacketBuilder':
        return self._encrypt(cipher)._addMac(proto_mac, hasher)

    def hash(self, hasher: hashes.HashAlgorithm) -> bytes:
        h = hashes.Hash(hasher)
        h.update(self.data())
        return h.finalize()
