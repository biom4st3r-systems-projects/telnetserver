from cryptography.hazmat.primitives.ciphers import CipherAlgorithm, algorithms, modes, Cipher, aead


class CipherWrapper:
    def __init__(self, cipher: Cipher) -> None:
        self.cipher = cipher
        self.encryptor = None
        self.decryptor = None

    def encrypt(self, data: bytes) -> bytes:
        if not self.encryptor:
            self.encryptor = self.cipher.encryptor()
        ct = self.encryptor.update(data)  # + self.encryptor.finalize()
        # self.encryptor = None
        return ct

    def decrypt(self, data: bytes) -> bytes:
        if not self.decryptor:
            self.decryptor = self.cipher.decryptor()
        pt = self.decryptor.update(data)  # + self.decryptor.finalize()
        # self.decryptor = None
        return pt
# eol
