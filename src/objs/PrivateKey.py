

from typing import Any, Union
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding, rsa, x25519, x448, ed25519, ed448
from cryptography.hazmat.primitives.serialization import Encoding, PublicFormat
from objs.Packets import PacketBuilder


class PrivateKey:
    def __init__(self, key: Union[rsa.RSAPrivateKeyWithSerialization, ed25519.Ed25519PrivateKey, ed448.Ed448PrivateKey]) -> None:
        self._key = key

    def getPublicKeyBytes(self) -> bytes:
        if isinstance(self._key, rsa.RSAPrivateKeyWithSerialization):
            return PacketBuilder().addString('ssh-rsa').addmpint(self._key.public_key().public_numbers().e).addmpint(self._key.public_key().public_numbers().n).data()
        elif isinstance(self._key, ed25519.Ed25519PrivateKey):
            return PacketBuilder().addString('ssh-ed25519').addString(self._key.public_key().public_bytes(encoding=Encoding.Raw, format=PublicFormat.Raw)).data()
        elif isinstance(self._key, ed448.Ed448PrivateKey):
            return PacketBuilder().addString('ssh-ed448').addString(self._key.public_key().public_bytes(encoding=Encoding.Raw, format=PublicFormat.Raw)).data()

    def sign(self, id: str, data: bytes, hasher: hashes.HashAlgorithm) -> bytes:
        builder = PacketBuilder().addString(id)
        if isinstance(self._key, rsa.RSAPrivateKeyWithSerialization):
            return builder.addString(self._key.sign(data, padding.PKCS1v15(), hasher)).data()
        else:
            return builder.addString(self._key.sign(data)).data()
