
class DecryptionError(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class MessageAuthenticationCodeError(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class NoMatchingAlgorithms(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class InvalidClientKexKey(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)
