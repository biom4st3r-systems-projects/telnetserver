from typing import Text, Union
from objs.Packets import Packet, PacketBuilder
import socket
import binascii
from enum import Enum
import os
import datetime

shouldLog = True


class logger:
    def __init__(self) -> None:
        self._log: str = ''

    def log(self, packet: Union[PacketBuilder, Packet], socket: socket.socket, stage: Enum) -> None:
        if isinstance(packet, PacketBuilder):
            self._log += f'SERVER {str(socket.getsockname())}'
            self._log += '\n'
            self._log += f'{stage.name} Id: {stage.value} Hex: {hex(stage.value)}'
            self._log += '\n'
            if packet._encrypted:
                self._log += '# cipher_text\n'
                self._log += self.bytes_to_hex(packet._encrypted)
                self._log += '\n'
                self._log += '# plain_text\n'
            self._log += self.bytes_to_hex(packet._data)
        else:
            self._log += f'CLIENT {str(socket.getpeername())}'
            self._log += '\n'
            self._log += f'{stage.name} Id: {stage.value} Hex: {hex(stage.value)}'
            self._log += '\n'
            if packet._encrypted:
                self._log += '# cipher_text\n'
                self._log += self.bytes_to_hex(packet._encrypted)
                self._log += '\n'
                self._log += '# plain_text\n'
            self._log += self.bytes_to_hex(packet._raw if packet._raw else packet._data.getvalue())
        self._log += '\n\n'

    def add_line(self, text: str) -> None:
        self._log += f'{text}\n'

    @staticmethod
    def bytes_to_hex(data: bytes):
        log = ''
        hex_bytes = []
        asciis = []
        data: bytes = binascii.hexlify(data)
        for i in range(0, len(data), 2):
            hex_bytes.append(data[i:i + 2].decode('utf-8'))
            try:
                s = binascii.unhexlify(data[i:i + 2]).decode('utf-8')
                if ord(s) < 0x20 or ord(s) > 0x7E:
                    raise Exception()
                asciis.append(s)
            except Exception:
                asciis.append('.')
        while len(hex_bytes) > 0:
            log += ' '.join(hex_bytes[0:16])
            log += '   '
            log += ''.join(asciis[0:16])
            log += '\n'
            hex_bytes = hex_bytes[16:]
            asciis = asciis[16:]
        return log

    def write(self) -> None:
        if not os.path.isdir('logs'):
            os.mkdir('logs')
        with open(f'logs/pyserver_log{datetime.datetime.now().strftime("_%d-%m-%Y_%H.%M.%S")}.log', 'w+') as file:
            file.write(self._log)
            self._log = ''


def getLogger() -> logger:
    if shouldLog:
        return logger()
    else:
        return DummyLogger()


class DummyLogger(logger):
    def write(self) -> None:
        pass

    def add_line(self, text: str) -> None:
        pass

    def log(self, packet: Union[PacketBuilder, Packet], socket: socket.socket, stage: Enum) -> None:
        pass


_logger = getLogger()
