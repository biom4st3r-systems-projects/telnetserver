from enum import Enum
from typing import Dict, Union, SupportsBytes
from objs.TypeCheck import checkType


class FakeStage():
    def __init__(self, name: str, value: int) -> None:
        self.name = name
        self.value = value


def between(val, min, max) -> bool:
    return val >= min and val < max


_lookup: Dict[int, 'Stage'] = None


class Stage(Enum):
    DISCONNECT = 1
    IGNORE = 2
    UNIMPLEMENTED = 3
    DEBUG = 4
    SERVICE_REQUEST = 5
    SERVICE_ACCEPT = 6
    KEX_INIT = 20
    NEWKEYS = 21
    SPECIFIC_KEX_INIT = 30
    SPECIFIC_KEX_REPLY = 31
    USER_AUTH_REQUEST = 50
    USER_AUTH_FAILURE = 51
    USER_AUTH_SUCCESS = 52
    USER_AUTH_BANNER = 53
    GLOBAL_REQUEST = 80
    REQUEST_SUCCESS = 81
    REQUEST_FAILURE = 82
    CHANNEL_OPEN = 90
    CHANNEL_OPEN_CONFIRM = 91
    CHANNEL_OPEN_FAILURE = 92
    CHANNEL_WINDOW_ADJUST = 93
    CHANNEL_DATA = 94
    CHANNEL_EXTENDED_DATA = 95
    CHANNEL_EOF = 96
    CHANNEL_CLOSE = 97
    CHANNEL_REQUEST = 98
    CHANNEL_SUCCESS = 99
    CHANNEL_FAILURE = 100
    HEADER = 10000
    EMPTY = 10001
    UNKNOWN = 10002

    @staticmethod
    def getStage(val: bytes) -> 'Stage':
        if len(val) == 0:
            return Stage.EMPTY
        identifier = val[5]
        if identifier in _lookup:
            return _lookup[identifier]
        elif val[:7] == b'SSH-2.0':
            return Stage.HEADER
        else:
            return Stage.UNKNOWN

    @staticmethod
    def getTypeByte(data: bytes) -> int:
        return data[5]


if not _lookup:
    _lookup = {}
    for x in Stage:
        _lookup[x.value] = x


class DISCONNECT_CODE(Enum):
    SSH_DISCONNECT_HOST_NOT_ALLOWED_TO_CONNECT = 1
    SSH_DISCONNECT_PROTOCOL_ERROR = 2
    SSH_DISCONNECT_KEY_EXCHANGE_FAILED = 3
    SSH_DISCONNECT_RESERVED = 4
    SSH_DISCONNECT_MAC_ERROR = 5
    SSH_DISCONNECT_COMPRESSION_ERROR = 6
    SSH_DISCONNECT_SERVICE_NOT_AVAILABLE = 7
    SSH_DISCONNECT_PROTOCOL_VERSION_NOT_SUPPORTED = 8
    SSH_DISCONNECT_HOST_KEY_NOT_VERIFIABLE = 9
    SSH_DISCONNECT_CONNECTION_LOST = 10
    SSH_DISCONNECT_BY_APPLICATION = 11
    SSH_DISCONNECT_TOO_MANY_CONNECTIONS = 12
    SSH_DISCONNECT_AUTH_CANCELLED_BY_USER = 13
    SSH_DISCONNECT_NO_MORE_AUTH_METHODS_AVAILABLE = 14
    SSH_DISCONNECT_ILLEGAL_USER_NAME = 15


class KeyType(Enum):
    IV_CLIENT = b'A'
    IV_SERVER = b'B'
    ENCRYPTION_CLIENT = b'C'
    ENCRYPTION_SERVER = b'D'
    INTEGRITY_CLIENT = b'E'  # MAC
    INTEGRITY_SERVER = b'F'  # MAC
