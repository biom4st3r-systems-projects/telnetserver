import socket
from concurrent.futures.thread import ThreadPoolExecutor
import traceback

from typing import Any, Iterable, List, Set, Dict, Tuple, Optional, Union

from objs.SSH_User import SSH_User

from objs.User import User

explitives_deleted = [b'']

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(('0.0.0.0', 1))
server.listen(5)
print(server.getsockname())

max_connections = 40


def ssh_bs(data: bytes, user: User):
    secure_user = SSH_User(user.socket)
    secure_user.bootstrap(data, user)


def getMessageType(data: bytes) -> str:
    x: int = data[5]
    if(x > 21):
        print('UNKNOWN %s' % x)
        return 'UNKNOWN %s' % x
    print(messageType[x])
    return messageType[x]


messageType = {
    1: 'SSH_MSG_DISCONNECT',
    2: 'SSH_MSG_IGNORE',
    3: 'SSH_MSG_UNIMPLEMENTED',
    4: 'SSH_MSG_DEBUG',
    5: 'SSH_MSG_SERVICE_REQUEST',
    6: 'SSH_MSG_SERVICE_ACCEPT',
    20: 'SSH_MSG_KEXINIT',
    21: 'SSH_MSG_NEWKEYS'
}


def task(user: User):
    try:
        # command_MOTD(user, '')
        # user.send_feedback("Welcome to the chat!\r\n")
        while True:
            data = user.listen(1024)
            if data[0:7] == b'SSH-2.0':
                ssh_bs(data, user)
                break
            # print(data)
            if not data:
                break
            else:
                user.update(data)
                if user.hasSentCommand():
                    user.tryCommand()
                elif user.hasReturned():
                    user.broadcast_msg()
                user.tryBackspace()
        user.socket.send(b'')
    except ConnectionResetError:
        print('ConnectionResetError')
    except ConnectionAbortedError:
        print('ConnectionAbortedError')
    except OSError as err:
        print(f'{err.errno} Socket probably died.')
    except Exception as err:
        print(traceback.format_exc())
        user.send_feedback("Exception has Occured. %s\r\n" % err)
    user.close()


def checkClientsStillConnected():
    for x in User.clients:
        user: User = x
        try:
            user.send_feedback('')
        except ConnectionAbortedError:
            user._disconnect()
        except Exception:  # noqa: E722
            print(traceback.format_exc())


with ThreadPoolExecutor(max_workers=max_connections) as exe:
    while True:
        print("waiting")
        (clientsocket, addr) = server.accept()

        user = User(clientsocket)

        checkClientsStillConnected()
        if len(User.clients) >= max_connections:
            user.send_feedback('Server Full!\r\n')
            user.socket.close()
        else:
            # User.clients.append(user)
            exe.map(task, [user])
